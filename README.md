# designSketch2normal
This code is a sligth modification of [sketch2normal](https://github.com/Ansire/sketch2normal).


## Usage
- Test:
```bash
python main.py --phase test --test_dir <path_to_dataset/test> --checkpoint_dir <path_to_checkpoint> --dataset_name <path_to_dataset> --results_dir <path_to_save_results>
```
<path_to_dataset/test> - the folder with all the test images.

- Visualize the training process:
```bash
cd logs
tensorboard --logdir=./
```

## Data

Testing data can be downloaded [here]().
Trained models for each of datasets can be download [here]().


## Acknowledgement
Thanks Bastien Wailly (bastien.wailly@inria.fr) for helping with preparation of training data.